const { Router } = require('express');

const router = new Router();

const Controller = require('../controller/company.controller');

router.post('/company', Controller.createObject);
router.get('/company/:id', Controller.getObject);
router.get('/company', Controller.getManyObject);
router.put('/company', Controller.updateObject);
router.delete('/company/:id', Controller.deleteObject);

module.exports = router;
