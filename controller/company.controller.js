const db = require('../db');

/// COMPANY ///
/// COMPANY ///

class companyController {
    async createObject(req, res) {
        const { companyID, name } = req.body;
        console.log(companyID, name);
        const newObject = await db.query(
            `INSERT INTO company (companyID, name) values ($1, $2) RETURNING *`,
            [companyID, name]
        );
        res.json(newObject.rows);
    }
    async getManyObjects(req, res) {
        const objects = await db.query('SELECT * FROM company');
        res.json(objects.rows);
    }
    async getObject(req, res) {
        const companyID = req.params.companyID;
        const object = await db.query(
            'SELECT * FROM company where companyID = $1',
            [companyID]
        );
        res.json(object.rows);
    }
    async updateObject(req, res) {
        const { companyID, name } = req.body;
        const object = await db.query(
            'UPDATE company set name = $1, where companyID = $2 RETURNING *',
            [name, companyID]
        );
        res.json(object.rows);
    }
    async deleteObject(req, res) {
        const companyID = req.params.companyID;
        const object = await db.query(
            'DELETE FROM company where companyID = $1',
            [companyID]
        );
        res.json(object.rows);
    }
}

module.exports = new companyController();
