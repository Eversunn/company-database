const db = require('../db');

/// document ///
/// document ///

class documentController {
    async createObject(req, res) {
        const { documentID, employeID, link, companyID } = req.body;
        console.log(documentID, employeID, link, companyID);
        const newObject = await db.query(
            `INSERT INTO document (documentID, employeID, link, companyID) values ($1, $2, $3, $4) RETURNING *`,
            [documentID, employeID, link, companyID]
        );
        res.json(newObject.rows);
    }
    async getManyObjects(req, res) {
        const objects = await db.query('SELECT * FROM document');
        res.json(objects.rows);
    }
    async getObject(req, res) {
        const documentID = req.params.documentID;
        const object = await db.query(
            'SELECT * FROM document where documentID = $1',
            [documentID]
        );
        res.json(object.rows);
    }
    async updateObject(req, res) {
        const { documentID, employeID, link, companyID } = req.body;
        const object = await db.query(
            'UPDATE document set employeID = $1, link = $2, companyID = $3 where documentID = $2 RETURNING *',
            [employeID, link, companyID, documentID]
        );
        res.json(object.rows);
    }
    async deleteObject(req, res) {
        const documentID = req.params.documentID;
        const object = await db.query(
            'DELETE FROM document where documentID = $1',
            [documentID]
        );
        res.json(object.rows);
    }
}

module.exports = new documentController();
