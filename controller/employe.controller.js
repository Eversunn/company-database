const db = require('../db');

/// EMPLOYE ///
/// EMPLOYE ///

class employeController {
    async createObject(req, res) {
        const { employeID, firstName, lastName, birthDate, email, companyID } =
            req.body;
        console.log(
            employeID,
            firstName,
            lastName,
            birthDate,
            email,
            companyID
        );
        const newObject = await db.query(
            `INSERT INTO employe (employeID, firstName,lastName,birthDate,email,companyID) values ($1, $2,3$,4$,5$,6$) RETURNING *`,
            [employeID, firstName, lastName, birthDate, email, companyID]
        );
        res.json(newObject.rows);
    }
    async getManyObjects(req, res) {
        const objects = await db.query('SELECT * FROM employe');
        res.json(objects.rows);
    }
    async getObject(req, res) {
        const employeID = req.params.employeID;
        const object = await db.query(
            'SELECT * FROM employe where employeID = $1',
            [employeID]
        );
        res.json(object.rows);
    }
    async updateObject(req, res) {
        const { employeID, firstName, lastName, birthDate, email, companyID } =
            req.body;
        const object = await db.query(
            'UPDATE employe set name = $1, $2, 3$, 4$, 5$ where employeID = $6 RETURNING *',
            [firstName, lastName, birthDate, email, companyID, employeID]
        );
        res.json(object.rows);
    }
    async deleteObject(req, res) {
        const employeID = req.params.employeID;
        const object = await db.query(
            'DELETE FROM employe where employeID = $1',
            [employeID]
        );
        res.json(object.rows);
    }
}

module.exports = new employeController();
