create TABLE company (
    companyID BIGSERIAL PRIMARY KEY
    name VARCHAR(255)
    );

create TABLE employe (
    employeID BIGSERIAL PRIMARY KEY
    firstName VARCHAR(255)
    lastName VARCHAR(255)
    birthDate DATE
    email VARCHAR(255)
    companyID BIGINT 
    FOREIGN KEY (companyID) REFERENCES company
    );
    
create TABLE document (
    documentID BIGSERIAL PRIMARY KEY,
    employeID BIGINT 
    link VARCHAR(255)
    FOREIGN KEY (employeID) REFERENCES employe
    companyID BIGINT 
    FOREIGN KEY (companyID) REFERENCES company
    );
